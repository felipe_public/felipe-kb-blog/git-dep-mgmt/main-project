﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="21008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="lib" Type="Folder">
			<Item Name="hse-logger.lvlib" Type="Library" URL="../lib/HSE-Logger/hse-logger.lvlib"/>
			<Item Name="Library A.lvlibp" Type="LVLibp" URL="../lib/Library A.lvlibp">
				<Item Name="Library A.lvclass" Type="LVClass" URL="../lib/Library A.lvlibp/Library A/Library A.lvclass"/>
			</Item>
			<Item Name="Library B.lvlib" Type="Library" URL="../lib/lib-b/Library B.lvlib"/>
			<Item Name="TDMS Headers.lvlib" Type="Library" URL="../lib/tdms-write-anything/TDMS Headers.lvlib"/>
		</Item>
		<Item Name="src" Type="Folder">
			<Item Name="MainProject.lvlib" Type="Library" URL="../src/MainProject/MainProject.lvlib"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
