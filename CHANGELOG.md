# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
### [1.1.1](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/compare/v1.1.0...v1.1.1) (2022-02-16)


### Bug Fixes

* **deps:** updates lib-a to PPL ([5211ef9](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/commit/5211ef9304183b53c3aafef5d9c7ece51086a8b5))

## [1.1.0](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/compare/v1.0.1...v1.1.0) (2022-02-16)


### Features

* adds two new deps with their features ([290cb88](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/commit/290cb8845ab21102613dbf5468de02968c8360af))

## [1.1.0](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/compare/v1.0.1...v1.1.0) (2022-02-16)


### Features

* adds two new deps with their features ([290cb88](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/commit/290cb8845ab21102613dbf5468de02968c8360af))

### [1.0.1](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/compare/v1.0.0...v1.0.1) (2022-02-16)


### Bug Fixes

* **deps:** adds version 2.0 of the lib-a sum package ([7d8c1b3](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/commit/7d8c1b3c9379afe3a2dde275e3ea0b486bf6ab5f))

## 1.0.0 (2022-02-16)


### Features

* adds dependencies file ([806732d](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/commit/806732d211e418612d785bae6e18d2e52be54d18))
* adds sum and multiply function ([f4c934b](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/commit/f4c934bb5e1e3fd753b23160cc48e93f95140613))


### Docs

* adds initial README documentation ([23cd39d](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/commit/23cd39dc2a76c41f73c6e55617bf051ee9ea0648))


### CI

* adds automatic versioning file ([2431bd3](https://gitlab.com/felipe_public/felipe-kb-blog/git-dep-mgmt/main-project/commit/2431bd3ce8f573f81b9e5da577f1d8505d1b0802))
